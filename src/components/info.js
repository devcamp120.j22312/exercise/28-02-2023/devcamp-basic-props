import { Component } from "react";

class Info extends Component {
    render() {
        return(
            <>
            My nam is {this.props.firstName} {this.props.lastName} and my favourite number is {this.props.favNumber}
            </>
        )
    }
}

export default Info;